from random import randint

guess_number = 1
name = input("Hi! What is your name?\n")

for i in range(5):
    month = str(randint(1, 12))
    year = str(randint(1924, 2004))
    if i < 5 :
        print("Guess " + str(guess_number) + " : " + name + " were you " +
        "born on " + month + " / " + year + " ?")
        response = input("yes/no\n")
        if response == "yes":
            print("I knew it!")
            break
        elif guess_number == 5 :
            print("I have other things to do. Good bye.")
        else :
            print("Drat! Lemme try again!")
    guess_number += 1
